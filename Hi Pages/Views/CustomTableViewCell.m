//
//  CustomTableViewCell.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell
@synthesize backgroundView;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:backgroundView.bounds];
    backgroundView.layer.masksToBounds = NO;
    backgroundView.layer.shadowColor = [UIColor blackColor].CGColor;
    backgroundView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    backgroundView.layer.shadowOpacity = 0.5f;
    backgroundView.layer.shadowPath = shadowPath.CGPath;
    
    UIBezierPath *shadowPath1 = [UIBezierPath bezierPathWithRect:_backView.bounds];
    _backView.layer.masksToBounds = NO;
    _backView.layer.shadowColor = [UIColor blackColor].CGColor;
    _backView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _backView.layer.shadowOpacity = 0.5f;
    _backView.layer.shadowPath = shadowPath1.CGPath;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
