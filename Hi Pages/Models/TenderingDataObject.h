//
//  TenderingDataObject.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 01/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TenderingDataObject : NSObject
@property (nonatomic, strong) NSString * TenderName, *TenderID, *TenderPagetitle , *TenderImage;
@property (nonatomic, strong) NSMutableArray * TenderDescription;

@end
