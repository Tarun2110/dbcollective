//
//  CategoryObject.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 26/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface CategoryObject : NSObject
@property (nonatomic, strong) NSString * categoryName, *categoryID, *categoryImage , *categoryChild, *unitStr;
@property (nonatomic, strong) NSString * address, *date, *landmark , *orderno, *servicename , *status, *time;


@property (nonatomic, strong) NSMutableArray *servicesArray;
@property (nonatomic, strong) NSString *selectdServiceName, *selectedServicePrice, *SelectedServiceUnit;

@property (nonatomic, strong) NSString * FinalPrice, *FinalData;



@property BOOL selected;

@end
