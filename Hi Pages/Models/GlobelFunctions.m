//
//  ScavengerHuntAPI.m
//  ScavengerHunt
//
//  Created by OSX on 10/03/15.
//  Copyright (c) 2015 Rohit. All rights reserved.
//

#import "GlobelFunctions.h"
//#import "SVProgressHUD.h"
//#import "SVRadialGradientLayer.h"


@implementation GlobelFunctions


//NSString const *BaseUrl2 = @"https://staging.docu.solutions/api/v2/";
//NSString const *BaseUrl = @"https://staging.docu.solutions/api/v1/";
NSString const *OAuthTokenUrl = @"https://staging.docu.solutions/oauth/token";

NSString * const kSortCreated = @"created";
NSString * const kSortDirection = @"DESC";


- (void)makeCallToNumber:(NSString *)phoneNumber
{
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
    
    if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
        [UIApplication.sharedApplication openURL:phoneUrl];
    } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
        [UIApplication.sharedApplication openURL:phoneFallbackUrl];
    } else {
        // Show an error message: Your device can not do phone calls.
    }
}



- (void) showLocationInAppleMap:(NSString *)location
{
//    NSString* addrss = [NSString stringWithFormat:@"Address: %@",location];
//    NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",addrss];
//    url =[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}



/**
 *Loaction Update did start method
 */
//- (void) LocationUpdate
//{
//    self.locationManager = [[CLLocationManager alloc] init];
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
//    
//    self.locationManager.delegate = self;
//    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
//        [self.locationManager requestWhenInUseAuthorization];
//    }
//    [self.locationManager startUpdatingLocation];
//}

/**
 *Loaction manager method
 */
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    NSString *latString;
//    NSString *longString;
//    CLLocation * currentLocation = [locations lastObject];
//    latString=[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
//    longString=[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
//    NSDate *date = [NSDate date];
//    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@,%@,%@",latString,longString,date] forKey:@"latLong"];
//}


/**
 *intiate keychain
 */
//-(void)ProgressStyle
//{
//    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeFlat];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//}
//
//
//#pragma mark - Predefined HUD's
//
//- (void)ShowProgress
//{
//    [self ProgressStyle];
//    [SVProgressHUD showWithStatus:@"Please Wait.."];
//}
//
//
//- (void)DismissProgress
//{
//    [self ProgressStyle];
//    [SVProgressHUD dismiss];
//}
//
//
//- (void)ErrorProgress
//{
//    [self ProgressStyle];
//    [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
//}
//


#pragma mark Device Types
/**
 *Device name from model name 
 */
+ (NSString *)getBaseUrl2 {
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"NewBaseUrl"] == nil) {
        return @"https://staging.docu.solutions/api/v2/";
    } else {
        return [[NSUserDefaults standardUserDefaults] stringForKey:@"NewBaseUrl"];
    }
}

- (NSString *) getDeviceModelName;
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    // Iphone's
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 2G";
    else if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    else if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    else if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    else if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
    else if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4";
    else if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    else if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    else if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5";
    else if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c";
    else if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c";
    else if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s";
    else if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s";
    else if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    else if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    else if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    else if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    else if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    else if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    else if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    else if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7";
    else if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7";
    else if ([platform isEqualToString:@"iPhone10,1"])   return @"iPhone 8";
    else if ([platform isEqualToString:@"iPhone10,4"])   return @"iPhone 8";
    else if ([platform isEqualToString:@"iPhone10,2"])   return @"iPhone 8";
    else if ([platform isEqualToString:@"iPhone10,5"])   return @"iPhone 8";
    else if ([platform isEqualToString:@"iPhone10,3"])   return @"iPhone X";
    else if ([platform isEqualToString:@"iPhone10,6"])   return @"iPhone X";

    // Ipod's
    else if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch (1 Gen)";
    else if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch (2 Gen)";
    else if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch (3 Gen)";
    else if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch (4 Gen)";
    else if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch (5 Gen)";
    
    // Ipad's
    else if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    else if ([platform isEqualToString:@"iPad1,2"])      return @"iPad 3G";
    else if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    else if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2";
    else if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    else if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2";
    else if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    else if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini";
    else if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    else if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    else if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    else if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3";
    else if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    else if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4";
    else if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    else if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air";
    else if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air";
    else if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2 (WiFi)";
    else if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2 (Cellular)";
    else if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2";
    else if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3";
    else if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3";
    else if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3";
    else if ([platform isEqualToString:@"iPad5,1"])      return @"iPad Mini 4 (WiFi)";
    else if ([platform isEqualToString:@"iPad5,2"])      return @"iPad Mini 4 (LTE)";
    else if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2";
    else if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2";
    else if ([platform isEqualToString:@"iPad6,1"])      return @"iPad Pro";
    else if ([platform isEqualToString:@"iPad6,2"])      return @"iPad Pro";
    else if ([platform isEqualToString:@"iPad6,3"])      return @"iPad Pro";
    else if ([platform isEqualToString:@"iPad6,4"])      return @"iPad Pro";
    else if ([platform isEqualToString:@"iPad6,5"])      return @"iPad Pro";
    else if ([platform isEqualToString:@"iPad6,6"])      return @"iPad Pro";
    else if ([platform isEqualToString:@"iPad6,7"])      return @"iPad Pro";
    else if ([platform isEqualToString:@"iPad6,8"])      return @"iPad Pro";
    
    else if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    else if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    else if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    
    else if ([platform isEqualToString:@"i386"])         return @"Simulator";
    else if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return platform;
}


- (NSString *) getCurrentTime
{
    NSDate *currentDate = [[NSDate alloc] init];
    
    NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
    // or Timezone with specific name like
    // [NSTimeZone timeZoneWithName:@"Europe/Vienna"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *localDateString = [dateFormatter stringFromDate:currentDate];
    
    
    return localDateString;

}


BOOL isStringEmpty(NSString *string)
{
    if([string length] == 0) { //string is empty or nil
        return YES;
    }
    if(![[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        //string is all whitespace
        return YES;
    }
    return NO;
}

BOOL isStringEqualZero(NSString *string)
{
    if ([string isEqualToString:@"0"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


- (NSString *)changeDateString:(NSString *)dateString;
{
    NSDate * dateToReturn;
    
    NSUserDefaults *data = [NSUserDefaults standardUserDefaults];
    NSString *storedTimeZone = [data objectForKey:@"SelectedTimeZone"];
    
    NSLog(@"%@",storedTimeZone);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:storedTimeZone];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.000Z"];
    
    dateToReturn = [dateFormatter dateFromString:dateString];
                    
    NSDateFormatter *isoDateFormatter = [NSDateFormatter new];
    [isoDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm"];
    NSString * string = [isoDateFormatter stringFromDate:dateToReturn];
    
    return string;
    
}


- (NSString *)changeDateStringWithSeconds:(NSString *)dateString;
{
    NSString *newString = [dateString substringToIndex:[dateString length]-3];
    
    NSUserDefaults *data = [NSUserDefaults standardUserDefaults];
    NSString *storedTimeZone = [data valueForKey:@"SelectedTimeZone"];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:storedTimeZone]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [formatter dateFromString:newString];
   
    NSDateFormatter *isoDateFormatter = [NSDateFormatter new];
    [isoDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm"];
    NSString * string1 = [isoDateFormatter stringFromDate:date];
    
    return string1;
}


- (NSString *)changeDateStringWithMiliSeconds:(NSString *)dateString;
{
    NSDate * dateToReturn;
    
    NSUserDefaults *data = [NSUserDefaults standardUserDefaults];
    NSString *storedTimeZone = [data objectForKey:@"SelectedTimeZone"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:storedTimeZone];
    [dateFormatter setTimeZone:timeZone];
   
    
    dateToReturn = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *isoDateFormatter = [NSDateFormatter new];
    [isoDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm"];
    NSString * string = [isoDateFormatter stringFromDate:dateToReturn];
    
    return string;
    
}


- (NSString *)changeDateStringWithMiliMiliSeconds:(NSString *)dateString;
{
    NSDate * dateToReturn;
    
    NSUserDefaults *data = [NSUserDefaults standardUserDefaults];
    NSString *storedTimeZone = [data objectForKey:@"SelectedTimeZone"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:storedTimeZone];
   
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS'Z'"];
    
    dateToReturn = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *isoDateFormatter = [NSDateFormatter new];
    [isoDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm"];
    NSString * string = [isoDateFormatter stringFromDate:dateToReturn];
    
    return string;
    
}



BOOL validateEmailWithString(NSString *emailText)
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9]+\\.[A-Za-z]{2,4}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailTest evaluateWithObject:emailText])
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9]+\\.[A-Za-z]{2,4}\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        NSLog(@"emailValid : %s",[emailTest evaluateWithObject:emailText]?"YES":"NO");
        return [emailTest evaluateWithObject:emailText];
    }
    NSLog(@"emailValid +: %s",[emailTest evaluateWithObject:emailText]?"YES":"NO");
    return [emailTest evaluateWithObject:emailText];
}


-(void)alert:(NSString *)alertMsg :(NSString *)message :(UIViewController *)TargetController
{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertMsg message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
   
    }];
    
    [alertController addAction:okAction];
    [TargetController presentViewController:alertController animated: YES completion: nil];
    
}

-(void)alertwithpop:(NSString *)alertMsg :(NSString *)message :(UIViewController *)TargetController
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertMsg message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   [TargetController.navigationController popViewControllerAnimated:YES];
                               }];
    
    [alertController addAction:okAction];
    [TargetController presentViewController:alertController animated: YES completion: nil];
    
}

- (void)addBaseLineBelowTextField :(NSString *)placeholder textfield:(UITextField *)textfield
{
    UIColor *color = [UIColor lightGrayColor];
    textfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, textfield.frame.size.height - 1, textfield.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [textfield.layer addSublayer: bottomBorder];
}




- (NSString *) getMimeTypeFunction:(NSString *)mimeType {
    
    NSString *type = nil;
    
    NSLog(@"Mime_type = %@",mimeType);
    
    NSArray *items = @[ @"image/jpeg", @"image/jpg", @"image/png", @"application/pdf", @"application/msword", @"application/vnd.openxmlformats-officedocument.wordprocessingml.document",@"application/vnd.openxmlformats-officedocument.wordprocessingml.template",@"application/vnd.ms-excel",@"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",@"application/csv",@"application/vnd.ms-powerpoint",@"application/vnd.openxmlformats-officedocument.presentationml.presentation",@"text/plain",@"application/zip",@"application/rar",@"application/x-zip",@"application/x-rar",@"application/x-zip-compressed",@"application/x-rar-compressed" ];
    
    for (int i = 0; i < items.count; i++)
    {
        if ([mimeType isEqualToString:items[i]])
        {
            NSUInteger Type = i;
            switch(Type)
            {
                {
                    // if Image
                case 0:
                case 1:
                    type = @"jpg";
                    break;
                case 2:
                    type = @"png";
                    break;
                    
                    // PDF
                case 3:
                    type = @"pdf";
                    break;
                    // Doc
                case 4:
                case 5:
                case 6:
                    type = @"doc";
                    break;
                    
                    // Excel/CSV
                case 7:
                case 8:
                case 9:
                    type= @"xlsx";
                    break;
                    // PPT
                case 10:
                case 11:
                    type = @"ppt";
                    break;
                    
                    // PPT
                case 12:
                    type = @"txt";
                    break;
                    
                    // ZIP
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                    type = @"zip";
                    break;
                default :
                    type = @"unknown";
                    break;
                }
            }
        }
    }
    NSLog(@"%@",type);
    return type;
}


- (void)openMailComposer :(UIViewController *)TargetController;
{
    }






@end
