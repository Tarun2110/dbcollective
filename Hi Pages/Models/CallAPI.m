#import "CallAPI.h"
#import "MPNotificationView.h"
@implementation CallAPI

@synthesize reachability;
@synthesize internetWorking;
@synthesize callBackSelector;
@synthesize callBackTarget;


- (id)init {
    if (self = [super init])
    {
        _GlobelFucntion = [GlobelFunctions new];
    }
    return self;
}

- (void) initAuthorizationFor:(NSString *)methodType
{
}

- (NSString *)setAccessToken
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
}




#pragma mark ********************
#pragma mark  Login API Functions
#pragma mark ********************

- (void)API_UserSignIn:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email :(NSString *)password :(NSString *)app_id :(NSString *)device_type :(NSString *)device_udid
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @
        {
            @"email": email,
            @"password": password,
            @"app_id": app_id,
            @"device_type": device_type,
            @"device_udid": device_udid,
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/signin",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
    
}





- (void)API_GetEvents:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/events",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


//#pragma mark **********************
//#pragma mark - Output Section
//#pragma mark **********************

- (void)handleOutputForOther:(NSMutableURLRequest *)urlRequest withTarget:(id)tempTarget withSelector:(SEL)tempSelector
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error)
                                          {
                                              NSLog(@"%@", error);
                                          }
                                          else
                                          {
                                              NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                              jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                              [self showOutPutHereWithTarget:tempTarget withSelector:tempSelector withDictionary:jsonResponse];
                                          }
                                      }];
    [dataTask resume];
}



- (void)showOutPutHereWithTarget:(id)tempTarget withSelector:(SEL)stateSelector  withDictionary:(NSDictionary *)dict
{
    [tempTarget performSelectorOnMainThread:stateSelector withObject:dict waitUntilDone:YES];
}

#pragma mark **********************
#pragma mark - Reachability Methods
#pragma mark **********************

- (void)checkInternetConnection
{
    reachability = [Reachability reachabilityForInternetConnection];
    [self performSelector:@selector(updateInterfaceWithReachability:)withObject:reachability];
}

- (void)updateInterfaceWithReachability:(Reachability *)curReach
{
    if(curReach == reachability)
    {
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        switch (netStatus)
        {
            case NotReachable:
            {
                internetWorking = 0;
                [MPNotificationView notifyWithText:@"No Network Connection"
                                            detail:@"Connect to a Wi-Fi network or a cellular data."
                                             image:nil
                                          duration:2.0
                                              type:@"Custom"
                                     andTouchBlock:^(MPNotificationView *notificationView)
                 {
                     NSLog( @"Received touch for notification with text: %@", notificationView.textLabel.text );
                 }];
                //  HideProgressHUD;
                break;
            }
            case ReachableViaWiFi:
            {
                internetWorking = 1;
                break;
            }
            case ReachableViaWWAN:
            {
                internetWorking = 1;
                break;
            }
        }
    }
}

-(void)noNetworkAlert
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowServerError" object:nil];
}

@end
