#import <Foundation/Foundation.h>
#import "GlobelFunctions.h"
#import "Reachability.h"

@class BaseViewController;


@interface CallAPI : NSObject<NSXMLParserDelegate>
{
    __weak NSString *deviceName;
    
    NSURLConnection *theConnection;
    NSMutableData   *mutResponseData;
    int             intResponseCode;
    Reachability    *reachability;
    NSString *str_currentElement;
    SEL callBackSelector;
    id __weak callBackTarget;
}


@property (strong ,nonatomic) BaseViewController *objBaseVC;
@property (strong,nonatomic) GlobelFunctions *GlobelFucntion;
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, readwrite) NSInteger internetWorking;
@property (nonatomic) SEL callBackSelector;
@property (nonatomic, weak) id callBackTarget;

- (void)checkInternetConnection;

//API FOR USER AUTHENTICATION//

- (void)API_UserSignIn:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email :(NSString *)password :(NSString *)app_id :(NSString *)device_type :(NSString *)device_udid;

- (void)API_GetEvents:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView;




@end
