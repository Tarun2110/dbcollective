//
//  JPBaseViewController.h
//  JobProgress
//
//  Created by Vishal on 15/04/16.
//  Copyright © 2016 Logiciel Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallAPI.h"
#import <MBProgressHUD/MBProgressHUD.h>


#define padding 5
#define labelwidthPadding 20
#define labelHeight 20

//Switch using For loop
#define SWITCH(StrToFind)               for (NSString *__StrToFind__ = StrToFind; ; )
//Matching case
#define CASE(StrToMatch)                 if ([__StrToFind__ containsString:StrToMatch])
//If no case found
#define DEFAULT

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define IPHONE    UIUserInterfaceIdiomPhone

#define ApplicationDelegate  ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define UserDefaults [NSUserDefaults standardUserDefaults]
#define WindowFrame  [UIScreen mainScreen].bounds
#define iOSVersion   [[[UIDevice currentDevice] systemVersion] floatValue]
#define Storyboard   [UIStoryboard storyboardWithName:@"Main" bundle: nil]
#define VCWithIdentifier(i) [Storyboard instantiateViewControllerWithIdentifier:i]

#define IS_IPHONE_6P ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667 ) < DBL_EPSILON)

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON)
#define IS_IPHONE_4 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON)



@class BaseViewController;

@protocol BaseControllerDelegate <NSObject>

- (void)okBtnPressed;
- (void)cancelBtnPressed;

@end

@interface BaseViewController : UIViewController


@property (nonatomic, strong) NSMutableArray *tokenNameArray;
@property (nonatomic, strong) NSMutableArray *tokenViews;

@property (nonatomic) UIFont *font;
@property (nonatomic) UIColor *whiteColor;

@property (nonatomic) UIColor *grayColor;
@property (nonatomic) CGRect ScreenSize;
@property (strong, nonatomic) NSBundle *localBundle;
@property (strong, nonatomic) CallAPI * APIService;
@property (strong, nonatomic) GlobelFunctions * GlobelFucntion;

@property (nonatomic, weak) id <BaseControllerDelegate> delgate;
@property (strong, nonatomic) UIAlertController* alertController;


#pragma mark globel functions

-(UIButton *)addRoundRectButtonForButton:(UIButton *)button withHeaderHeight:(CGFloat)headerHeight;
-(void)saveObjects:(NSArray *)objects ToUserDefaultWith:(NSString *)name;
-(void)ShowProgress;
-(void)HideProgress;


- (NSMutableArray *)getValuesFromUserDefaultWith:(NSString *)name;

@end
