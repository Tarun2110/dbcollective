//
//  BookingViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 08/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "ExportViewController.h"
#import "CustomTableViewCell.h"
#import <MessageUI/MessageUI.h>

@interface ExportViewController ()
@property (assign) NSInteger expandedHeaderNumber;
@property (assign) UITableViewHeaderFooterView *expandedHeader;

@end

@implementation ExportViewController
{
    int i;
    NSString *filePath;
    NSMutableArray *employeeInfoArray;
}


#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    i = 0;
    _bookingTableView.delegate = self;
    _bookingTableView.dataSource = self;
    employeeInfoArray = [NSMutableArray new];
    
    self.bookingTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.bookingTableView.rowHeight = UITableViewAutomaticDimension;
    self.bookingTableView.estimatedRowHeight = 100;
    self.expandedHeaderNumber = -1;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *VideoCellIdentifier = @"CustomEventCell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil]objectAtIndex:0];
    }
    if (indexPath.row == 0)
    {
        cell.titleLabel.text = @"Email CSV";
    }
    else if (indexPath.row == 1)
    {
     cell.titleLabel.text = @"Sync to Server";
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"filterDict"];
    
    NSArray*values = [[dataDict allValues] objectAtIndex:0];
    
    NSArray*keys=[dataDict allValues];

    
    
    for (int i =0; i<keys.count; i++)
    {
        NSString *Name  = [[[keys objectAtIndex:i] valueForKey:@"name"] objectAtIndex:0];
        NSString *phone  = [[[keys objectAtIndex:i] valueForKey:@"phone"] objectAtIndex:0];
        NSString *email  = [[[keys objectAtIndex:i] valueForKey:@"email"] objectAtIndex:0];
        NSString *eventname  = [[[keys objectAtIndex:i] valueForKey:@"filterName"] objectAtIndex:0];

        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:0];
        [dict setValue:[NSString stringWithFormat:@"%@",Name] forKey:@"Name"];
        [dict setValue:[NSString stringWithFormat:@"%@",phone] forKey:@"Phoneno"];
        [dict setValue:[NSString stringWithFormat:@"%@",email] forKey:@"Email"];
        [dict setValue:[NSString stringWithFormat:@"%@",eventname] forKey:@"filterName"];

        [employeeInfoArray addObject:dict];
    }

    if (indexPath.row == 0)
    {
        if (dataDict)
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
                [composeViewController setMailComposeDelegate:self];
                [composeViewController setToRecipients:@[@"user@email.com"]];
                [composeViewController setSubject:@"Data CSV Attached"];
                [self presentViewController:composeViewController animated:YES completion:nil];
               
                [self createCSV];

                NSData *myData = [NSData dataWithContentsOfFile:filePath];
                if (myData)
                {
                    [composeViewController addAttachmentData:myData  mimeType:@"text/cvs" fileName:@"EmployeeRecords.csv"];
                }
            }
        }
    }
    else if (indexPath.row == 1)
    {
        if (dataDict)
        {
            [self ShowProgress];
            [self createCSV];
            [self JsonParsingPostMetod];
        }
    }
}

-(void)createCSV
{
    NSMutableString *csvString = [[NSMutableString alloc]initWithCapacity:0];
    [csvString appendString:@"Name,Phoneno,Email,Event,\n\n\n\n"];
   
    for (NSDictionary *dct in employeeInfoArray)
    {
        [csvString appendString:[NSString stringWithFormat:@"%@,%@,%@,%@, \n",[dct valueForKey:@"Name"],[dct valueForKey:@"Phoneno"],[dct valueForKey:@"Email"],[dct valueForKey:@"filterName"]]];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"EmployeeRecords.csv"];
    [csvString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}


#pragma mark  CallingAPI

- (void)JsonParsingPostMetod
{
    NSString *userid = [UserDefaults valueForKey:@"id"];

    //-- Convert string into URL
    NSString *urlString = [NSString stringWithFormat:@"http://dbcollectiveapi.seraphicinfotech.com/api/savedata"];
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    //-- Append data into posr url using following method
    NSMutableData *body = [NSMutableData data];
    
    //-- For Sending text
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",userid] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if (filePath)
    {
        NSString *filename  = @"EmployeeRecords.csv";
        NSData   *data      = [NSData dataWithContentsOfFile:filePath];
        NSString *mimetype  = @".csv/text";
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"import_file\"; filename=\"%@\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:data];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    }
    [request setHTTPBody:body];
    
    NSURLSessionDataTask * dataTask =[[NSURLSession sharedSession]
                                      dataTaskWithRequest:request
                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                      {
                                          if(data == nil)
                                          {
                                              return;
                                          }
                                          NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                          jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self HideProgress];
                                              int  successString = [[jsonResponse valueForKey:@"status"] intValue];
                                              if (successString == 1)
                                              {
                                                  //LoginClass * controller = VCWithIdentifier(@"EnterOTPViewController");
                                                  //[self.navigationController pushViewController:controller animated:YES];
                                              }
                                              else
                                              {
                                                  [self.GlobelFucntion alert:@"Alert!" :@"Something went wrong" :self];
                                              }
                                          });
                                      }];
    [dataTask resume];
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
