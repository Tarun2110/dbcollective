//
//  MakeBookingViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 02/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>


@interface CameraViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,AVCaptureMetadataOutputObjectsDelegate>
{
    AVCaptureStillImageOutput *stillImageOutput;
    NSString *FilterImage;
    NSString *FilterName,*FilterId;
    
    __weak IBOutlet UIImageView *CapturedImage;
}
@property (weak, nonatomic) IBOutlet UIImageView *BackgroundFilterImage;
@property (strong, nonatomic) IBOutlet UITableView *filterTableView;
@property (strong, nonatomic) IBOutlet UIView *ScanView;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *backgroundView2;
@property (strong, nonatomic) IBOutlet UIView *QrCode;
@property (strong, nonatomic) IBOutlet UIView *camscanView;
@property (strong, nonatomic) IBOutlet UITextField *nameTxt;
@property (strong, nonatomic) IBOutlet UITextField *phoneTxt;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UIButton *CancelDoneButton;
@property (strong, nonatomic) IBOutlet UIView *CameraView;

@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *ShutterButton;
@property (weak, nonatomic) IBOutlet UIView *CameraOptionsView;
@property (weak, nonatomic) IBOutlet UIButton *headerSaveButton;

@property (weak, nonatomic) IBOutlet UIView *BaseView;
@property (weak, nonatomic) IBOutlet UIView *HeaderView;


@end
