//
//  ConfirmBookingController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "ChangePasswordController.h"


@interface ChangePasswordController ()
@end
@implementation ChangePasswordController
{
  
}

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_BackView.bounds];
    _BackView.layer.masksToBounds = NO;
    _BackView.layer.shadowColor = [UIColor blackColor].CGColor;
    _BackView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _BackView.layer.shadowOpacity = 0.5f;
    _BackView.layer.shadowPath = shadowPath.CGPath;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
