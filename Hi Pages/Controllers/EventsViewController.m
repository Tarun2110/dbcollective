//  HomeViewController.m
//  Hi Pages
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.


#import "EventsViewController.h"
#import "CustomTableViewCell.h"
#import "EventDetailController.h"

@interface EventsViewController ()
@end
@implementation EventsViewController
{
    NSArray * FilterArray;
    NSDictionary *dataDict;
}
#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self ShowProgress];
    [self.APIService API_GetEvents:self withSelector:@selector(OutputForEvents:) :self.view];
}


- (void) OutputForEvents:(NSDictionary *)responseDict
{
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    
    if (successString == 1)
    {
        FilterArray = [responseDict valueForKey:@"response"];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:FilterArray forKey:@"FilterArray"];
        [userDefaults synchronize];
        
        EventTableView.delegate = self;
        EventTableView.dataSource = self;
        [EventTableView reloadData];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [self HideProgress];
    dataDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"filterDict"];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return FilterArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *VideoCellIdentifier = @"CustomEventCell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil]objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.titleLabel.text =  [NSString stringWithFormat:@"%@",[[FilterArray valueForKey:@"name"]objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *name =  [NSString stringWithFormat:@"%@",[[FilterArray valueForKey:@"name"]objectAtIndex:indexPath.row]];
    NSMutableArray *photoArray = [dataDict valueForKey:name];
    
    if (photoArray.count>0)
    {
        EventDetailController * controller = VCWithIdentifier(@"EventDetailController");
        controller.picdataArray = photoArray;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        [self.GlobelFucntion alert:@"Alert!" :@"No Photos" :self];
    }
    
 
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
