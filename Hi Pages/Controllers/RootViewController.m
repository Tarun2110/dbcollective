//  ViewController.m
//  Hi Pages
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.

#import "RootViewController.h"
#import "LoginClass.h"
#import "EventsViewController.h"

@interface RootViewController ()
@end
@implementation RootViewController
{
    NSData *imageData;
}
#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_fieldview.bounds];
    _fieldview.layer.masksToBounds = NO;
    _fieldview.layer.shadowColor = [UIColor blackColor].CGColor;
    _fieldview.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _fieldview.layer.shadowOpacity = 0.5f;
    _fieldview.layer.shadowPath = shadowPath.CGPath;
    
    UIBezierPath *shadowPath1 = [UIBezierPath bezierPathWithRect:_profilepic.bounds];
    _profilepic.layer.masksToBounds = NO;
    _profilepic.layer.shadowColor = [UIColor blackColor].CGColor;
    _profilepic.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _profilepic.layer.shadowOpacity = 0.5f;
    _profilepic.layer.shadowPath = shadowPath1.CGPath;
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [_profilepic addGestureRecognizer:tapGesture1];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:true];
    
}

#pragma mark  TappedActions

- (IBAction)TappedAction_SignUp:(id)sender
{
    if (isStringEmpty(_nameTextField.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your name" :self];
    }
    else if (isStringEmpty(_phonenumberTextField.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter your contact number" :self];
    }
    else if (!validateEmailWithString(_emailTextField.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter your valid email address" :self];
    }
    else if (isStringEmpty(_passwordTextField.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter password" :self];
    }
    else
    {
        [self ShowProgress];
        [self simpleJsonParsingPostMetod];
    }
}

- (IBAction)TappedAction_LoginClass:(id)sender
{
    LoginClass * controller = VCWithIdentifier(@"EnterOTPViewController");
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)TappedAction_SkipButton:(id)sender
{
    [UserDefaults setBool:YES forKey:@"skipped"];
    obj_delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [obj_delegate addTabbar];
    EventsViewController * controller  = VCWithIdentifier(@"EventsViewController");
    [self.navigationController pushViewController:controller animated:NO];
}



#pragma mark  Setting ProfileImage

- (void) tapGesture: (id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _profilepic.image = chosenImage;
    _profilepic.contentMode = UIViewContentModeScaleToFill;
    imageData = UIImageJPEGRepresentation(chosenImage, 0.8);
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark  CallingAPI

- (void)simpleJsonParsingPostMetod
{
    NSString *name = _nameTextField.text;
    NSString *email =  _emailTextField.text;
    NSString *phoneno = _phonenumberTextField.text;
    NSString *password = _passwordTextField.text;
    NSString *profile_image = @"test.png";
    NSString *app_id = @"1";
    NSString *device_udid = [UserDefaults valueForKey:@"UDID"];
    NSString *device_type = @"ios";
    
    //-- Convert string into URL
    NSString *urlString = [NSString stringWithFormat:@"http://dbcollectiveapi.seraphicinfotech.com/api/signup"];
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    //-- Append data into posr url using following method
    NSMutableData *body = [NSMutableData data];
    
    //-- For Sending text
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",name] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",email] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"phoneno"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",phoneno] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"password"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",password] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"app_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",app_id] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"device_udid"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",device_udid] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"device_type"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",device_type] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"profile_image\"; filename=\"%@\"\r\n",profile_image] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [request setHTTPBody:body];
    
    NSURLSessionDataTask * dataTask =[[NSURLSession sharedSession]
                                      dataTaskWithRequest:request
                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                      {
                                          if(data == nil)
                                          {
                                              return;
                                          }
                                          NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                          jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self HideProgress];
                                              int  successString = [[jsonResponse valueForKey:@"status"] intValue];
                                              if (successString == 1)
                                              {
                                                  LoginClass * controller = VCWithIdentifier(@"EnterOTPViewController");
                                                  [self.navigationController pushViewController:controller animated:YES];
                                              }
                                              else
                                              {
                                                  [self.GlobelFucntion alert:@"Alert!" :@"Something went wrong" :self];
                                              }
                                          });
                                      }];
    [dataTask resume];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

