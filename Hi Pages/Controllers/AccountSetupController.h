//
//  EditProfileViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AccountSetupController : BaseViewController<UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;

@property (strong, nonatomic) IBOutlet UITextField *nameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *phonenumberTextfield;
@property (strong, nonatomic) IBOutlet UITextField *emailTextfield;




@end
