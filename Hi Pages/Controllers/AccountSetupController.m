//
//  EditProfileViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "AccountSetupController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AccountSetupController ()
@end
@implementation AccountSetupController
{
    NSData *imageData;
}
#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tabBarController.tabBar setHidden:YES];

    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_backgroundView.bounds];
    _backgroundView.layer.masksToBounds = NO;
    _backgroundView.layer.shadowColor = [UIColor blackColor].CGColor;
    _backgroundView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _backgroundView.layer.shadowOpacity = 0.5f;
    _backgroundView.layer.shadowPath = shadowPath.CGPath;
    
    UIBezierPath *shadowPath1 = [UIBezierPath bezierPathWithRect:_profilePic.bounds];
    _profilePic.layer.masksToBounds = NO;
    _profilePic.layer.shadowColor = [UIColor blackColor].CGColor;
    _profilePic.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _profilePic.layer.shadowOpacity = 0.5f;
    _profilePic.layer.shadowPath = shadowPath1.CGPath;
    
    _emailTextfield.text = [UserDefaults valueForKey:@"email"];
    _nameTextfield.text = [UserDefaults valueForKey:@"name"];
    _phonenumberTextfield.text = [UserDefaults valueForKey:@"phoneno"];
    
    NSString* profileLink = [UserDefaults valueForKey:@"profile_image"];
    [_profilePic sd_setImageWithURL:[NSURL URLWithString:profileLink]
                 placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    _profilePic.contentMode = UIViewContentModeScaleToFill;

    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [_profilePic addGestureRecognizer:tapGesture1];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:true];
    
}

#pragma mark  Setting ProfileImage

- (void) tapGesture: (id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _profilePic.image = chosenImage;
    _profilePic.contentMode = UIViewContentModeScaleToFill;
    imageData = UIImageJPEGRepresentation(chosenImage, 0.8);
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark  TappedActions

- (IBAction)TappedAction_Back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)TappedAction_SaveButton:(id)sender
{
    if (!(validateEmailWithString(_emailTextfield.text)))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your valid email" :self];
    }
    else if (isStringEmpty(_phonenumberTextfield.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your contact number" :self];
    }
    else if (isStringEmpty(_nameTextfield.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter your name" :self];
    }
    else
    {
        [self ShowProgress];
        //[self SendMultiFormData];
    }
}

#pragma mark  CallingAPI
//
//- (void)SendMultiFormData
//{
//
//
//    NSString *name = nameTextfield.text;
//    NSString *email =  _emailTextField.text;
//    NSString *phoneno = phonenumberTextfield.text;
//    NSString *password = _passwordTextField.text;
//    NSString *profile_image = @"test.png";
//    NSString *app_id = @"1";
//    NSString *device_udid = [UserDefaults valueForKey:@"UDID"];
//    NSString *device_type = @"ios";
//
//    //-- Convert string into URL
//    NSString *urlString = [NSString stringWithFormat:@"http://dbcollectiveapi.seraphicinfotech.com/api/signup"];
//    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:urlString]];
//    [request setHTTPMethod:@"POST"];
//
//    NSString *boundary = @"14737809831466499882746641449";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//    //-- Append data into posr url using following method
//    NSMutableData *body = [NSMutableData data];
//
//    //-- For Sending text
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"name"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",name] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"email"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",email] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"phoneno"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",phoneno] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"password"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",password] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"app_id"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",app_id] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"device_udid"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",device_udid] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"device_type"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",device_type] dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//    if (imageData)
//    {
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"profile_image\"; filename=\"%@\"\r\n",profile_image] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[NSData dataWithData:imageData]];
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
//    [request setHTTPBody:body];
//
//    NSURLSessionDataTask * dataTask =[[NSURLSession sharedSession]
//                                      dataTaskWithRequest:request
//                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
//                                      {
//                                          if(data == nil)
//                                          {
//                                              return;
//                                          }
//                                          NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
//                                          jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
//
//                                          dispatch_async(dispatch_get_main_queue(), ^{
//                                              [self HideProgress];
//                                              int  successString = [[jsonResponse valueForKey:@"status"] intValue];
//                                              if (successString == 1)
//                                              {
//                                                  LoginClass * controller = VCWithIdentifier(@"EnterOTPViewController");
//                                                  [self.navigationController pushViewController:controller animated:YES];
//                                              }
//                                              else
//                                              {
//                                                  [self.GlobelFucntion alert:@"Alert!" :@"Something went wrong" :self];
//                                              }
//                                          });
//                                      }];
//    [dataTask resume];
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
