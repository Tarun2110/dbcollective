//
//  EnterOTPViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AppDelegate.h"

@interface LoginClass : BaseViewController <UITextFieldDelegate>
{
    AppDelegate *obj_delegate;
    IBOutlet UITextField *emailtxtfield;
    IBOutlet UITextField *passwordtxtfield;
}

@property (strong, nonatomic) IBOutlet UIView *fieldView;

@end
