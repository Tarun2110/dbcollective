//
//  TrendingViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface EventDetailController : BaseViewController <UICollectionViewDataSource,UICollectionViewDelegate>

{
    UIImage *img;
}
@property (weak, nonatomic) IBOutlet UICollectionView *TrendingCollectioView;
@property (strong, nonatomic) NSMutableArray *picdataArray;
@property (weak, nonatomic) IBOutlet UIView *fullView;
@property (weak, nonatomic) IBOutlet UIImageView *picturePreview;

@end
