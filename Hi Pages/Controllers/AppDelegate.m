//
//  AppDelegate.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "AppDelegate.h"
#import "EventsViewController.h"
#import "RootViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate
UINavigationController *navController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"LoggedIn"])
    {
        EventsViewController *HomeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EventsViewController"]; //or the homeController
        navController = [[UINavigationController alloc]initWithRootViewController:HomeVC];
        self.window.rootViewController = navController;
        [self.window addSubview:navController.view];
        [self addTabbar];
    }
    else
    {
        RootViewController *RootController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RootViewController"];
        navController = [[UINavigationController alloc]initWithRootViewController:RootController];
        self.window.rootViewController = navController;
    }
    [self.window makeKeyAndVisible];

    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"output is : %@", Identifier);
    
    
    [UserDefaults setValue:Identifier forKey:@"UDID"];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (void)addTabbar
{
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    
    UIViewController *viewController1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EventsViewController"];
    UIViewController *viewController2 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ContactsViewController"];
    UIViewController *viewController3 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CameraViewController"];
    UIViewController *viewController4 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ExportViewController"];
    UIViewController *viewController5 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    UINavigationController *navigationController1 = [[UINavigationController alloc] initWithRootViewController:viewController1];
    UINavigationController *navigationController2 = [[UINavigationController alloc] initWithRootViewController:viewController2];
    UINavigationController *navigationController3 = [[UINavigationController alloc] initWithRootViewController:viewController3];
    UINavigationController *navigationController4 = [[UINavigationController alloc] initWithRootViewController:viewController4];
    UINavigationController *navigationController5 = [[UINavigationController alloc] initWithRootViewController:viewController5];
    
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:navigationController1,navigationController2, navigationController3,navigationController4,navigationController5,nil];

    _tabBar = _tabBarController.tabBar;
    UIImageView *tabBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.window.bounds.size.width, 50)];
    
    tabBackground.backgroundColor = [UIColor blackColor];
    tabBackground.contentMode = UIViewContentModeScaleAspectFill;
    [self.tabBar insertSubview:tabBackground atIndex:0];
    
    UITabBarItem *tabBarItem1 = [_tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [_tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [_tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [_tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [_tabBar.items objectAtIndex:4];
    
     UIImage *active_image   = [UIImage imageNamed:@"icn_tap_bar_event_selected"];
     UIImage *inactive_image = [UIImage imageNamed:@"icn_tap_bar_event_normal"];
    tabBarItem1.imageInsets = UIEdgeInsetsMake(2, 0, -2, 0);

    [tabBarItem1 setImage:[inactive_image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setSelectedImage:[active_image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem1.title = @"Events";
    tabBarItem1.tag = 1;
    
    [tabBarItem2 setImage:[[UIImage imageNamed:@"icn_tap_bar_contacts_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setSelectedImage:[[UIImage imageNamed:@"icn_tap_bar_contacts_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem2.imageInsets = UIEdgeInsetsMake(2, 0, -2, 0);

    tabBarItem2.title = @"Contacts";
    tabBarItem2.tag = 2;
    
    [tabBarItem3 setImage:[[UIImage imageNamed:@"icn_tap_bar_scanner"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setSelectedImage:[[UIImage imageNamed:@"icn_tap_bar_scanner"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    tabBarItem3.title = @"";
    
    tabBarItem3.imageInsets = UIEdgeInsetsMake(2, 0, -2, 0);
    tabBarItem3.tag = 3;
    
    [tabBarItem4 setImage:[[UIImage imageNamed:@"icn_tap_bar_ecport_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem4 setSelectedImage:[[UIImage imageNamed:@"icn_tap_bar_ecport_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem4.imageInsets = UIEdgeInsetsMake(2, 0, -2, 0);

    tabBarItem4.title = @"Export";
    tabBarItem4.tag = 4;
  
    [tabBarItem5 setImage:[[UIImage imageNamed:@"icn_tap_bar_settings_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem5 setSelectedImage:[[UIImage imageNamed:@"icn_tap_bar_settings_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem5.imageInsets = UIEdgeInsetsMake(2, 0, -2, 0);
    tabBarItem5.title = @"Settings";
    tabBarItem5.tag = 5;
    
    [self.tabBar setTintColor:[UIColor whiteColor]];
    [self.tabBar setBarTintColor:[UIColor whiteColor]];
    
    _BackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,80, 80)];
    _BackgroundView.backgroundColor = [UIColor whiteColor];
    _BackgroundView.layer.cornerRadius = _BackgroundView.frame.size.height / 2;
    _BackgroundView.tag = 1000;
    _BackgroundView.clipsToBounds = YES;
    
    UIImageView *centerImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
    centerImage.image = [UIImage imageNamed:@"icn_tap_bar_scanner"];
    centerImage.contentMode  = UIViewContentModeScaleAspectFit;
    [_BackgroundView addSubview:centerImage];
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGestureCenter:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [_BackgroundView addGestureRecognizer:tapGesture1];
    
    CGFloat heightDifference = _BackgroundView.frame.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0)
        _BackgroundView.center = self.tabBar.center;
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        _BackgroundView.center = center;
    }
    
    [self.tabBarController.view addSubview:_BackgroundView];
    self.window.rootViewController = self.tabBarController;
}

- (void) tapGestureCenter: (id)sender
{
    [self.tabBarController setSelectedIndex:2];
}


@end
