//
//  ProfileViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "ProfileViewController.h"
#import "CustomTableViewCell.h"
#import "ChangePasswordController.h"
#import "AccountSetupController.h"
#import "RootViewController.h"
#import "LoginClass.h"


@interface ProfileViewController ()
@end
@implementation ProfileViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
  //  [self ShowProgress];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    SettingTableView.delegate = self;
    SettingTableView.dataSource = self;
    [self.tabBarController.tabBar setHidden:NO];
    [SettingTableView reloadData];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *VideoCellIdentifier = @"CustomEventCell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil]objectAtIndex:0];
    }

    if (![UserDefaults boolForKey:@"skipped"])
    {
        if (indexPath.row == 0)
        {
            cell.backgroundView.alpha = 0.5;
        }
    }
    
    if (indexPath.row == 0)
    {
        cell.titleLabel.text = @"Login";
    }
    else if (indexPath.row == 1)
    {
        cell.titleLabel.text = @"Change Password";
    }
    else if (indexPath.row == 2)
    {
        cell.titleLabel.text = @"Account Setup";
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if ([UserDefaults boolForKey:@"skipped"])
        {
            LoginClass * controller = VCWithIdentifier(@"EnterOTPViewController");
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
   else if (indexPath.row == 1)
    {
        ChangePasswordController * controller = VCWithIdentifier(@"ChangePasswordController");
        [self.navigationController pushViewController:controller animated:YES];
    }
   else if (indexPath.row == 2)
    {
        AccountSetupController * controller = VCWithIdentifier(@"AccountSetupController");
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (IBAction)TappedAction_logout:(id)sender
{
    [UserDefaults setBool:NO forKey:@"LoggedIn"];
    [self.tabBarController.tabBar setHidden:YES];
    RootViewController * controller = VCWithIdentifier(@"RootViewController");
    [self.navigationController pushViewController:controller animated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
