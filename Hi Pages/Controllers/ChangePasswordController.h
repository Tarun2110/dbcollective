//
//  ConfirmBookingController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface ChangePasswordController : BaseViewController
{
    IBOutlet UITextField *oldPassTextfield;
    IBOutlet UITextField *newPasswordTextfield;
    IBOutlet UITextField *confirmPassTextField;
}
@property (strong, nonatomic) IBOutlet UIView *BackView;



@end
