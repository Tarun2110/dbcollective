//  EnterOTPViewController.m
//  Hi Pages
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.

#import "LoginClass.h"
#import "EventsViewController.h"

@interface LoginClass ()
@end
@implementation LoginClass

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_fieldView.bounds];
    _fieldView.layer.masksToBounds = NO;
    _fieldView.layer.shadowColor = [UIColor blackColor].CGColor;
    _fieldView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _fieldView.layer.shadowOpacity = 0.5f;
    _fieldView.layer.shadowPath = shadowPath.CGPath;
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];

}

#pragma mark  Textfield Delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length < 1) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        return NO;
        
    }else if ((textField.text.length >= 1) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        return NO;
    }
    else if ((textField.text.length >= 1) && (string.length == 0)){
        NSInteger prevTag = textField.tag - 1;
        UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
        if (! prevResponder)
        {
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (prevResponder)
            [prevResponder becomeFirstResponder];
        return NO;
    }
    return YES;
}


- (IBAction)TappedAction_Signup:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark  ProceedButton Action

- (IBAction)TappedAction_SignIn:(id)sender
{
    if (!validateEmailWithString(emailtxtfield.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter your valid email" :self];
    }
    else if (isStringEmpty(passwordtxtfield.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Enter your password" :self];
    }
    else
    {
        [self ShowProgress];
        NSString *device_udid = [UserDefaults valueForKey:@"UDID"];
        [self.APIService API_UserSignIn:self withSelector:@selector(OutputForOTP:) :self.view :emailtxtfield.text :passwordtxtfield.text :@"1" :@"ios" :device_udid];
    }
}


- (void) OutputForOTP:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        NSString *phoneNumber = [[responseDict valueForKey:@"data"] valueForKey:@"phoneno"];
        NSString *propic = [[responseDict valueForKey:@"data"] valueForKey:@"profile_image"];
        NSString *email = [[responseDict valueForKey:@"data"] valueForKey:@"email"];
        NSString *name = [[responseDict valueForKey:@"data"] valueForKey:@"name"];
        NSString *userId = [[responseDict valueForKey:@"data"] valueForKey:@"id"];
        
        [UserDefaults setValue:phoneNumber forKey:@"phoneno"];
        [UserDefaults setValue:propic forKey:@"profile_image"];
        [UserDefaults setValue:email forKey:@"email"];
        [UserDefaults setValue:name forKey:@"name"];
        [UserDefaults setValue:userId forKey:@"id"];

        if ([UserDefaults boolForKey:@"skipped"])
        {
            [self.navigationController popViewControllerAnimated:YES];
            [UserDefaults setBool:NO forKey:@"skipped"];
        }
        else
        {
            obj_delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            [obj_delegate addTabbar];
            [UserDefaults setBool:YES forKey:@"LoggedIn"];
            EventsViewController * controller  = VCWithIdentifier(@"EventsViewController");
            [self.navigationController pushViewController:controller animated:YES];
            [UserDefaults setBool:NO forKey:@"skipped"];
        }
    }
    else
    {
        [self HideProgress];
        [self.GlobelFucntion alert:@"Alert!" :@"Something went wrong." :self];
    }
}

-(void) navigateToHome
{
    [self HideProgress];
    obj_delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [obj_delegate addTabbar];
    [UserDefaults setBool:YES forKey:@"LoggedIn"];
    EventsViewController * controller  = VCWithIdentifier(@"EventsViewController");
    [self.navigationController pushViewController:controller animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
