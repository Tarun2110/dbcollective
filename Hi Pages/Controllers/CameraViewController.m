//
//  MakeBookingViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 02/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "CameraViewController.h"
#import "CustomTableViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
BOOL Front;
BOOL torchIsOn;
NSMutableDictionary * dict;
AppDelegate *obj_delegate;

@interface CameraViewController ()
@property (nonatomic) BOOL isReading;
@property (nonatomic) BOOL TakePhoto;


-(BOOL)startReading;
-(void)stopReading;

@end

@implementation CameraViewController
{
    NSArray * FilterArray;
    int Code;
    int PicFrame, imagecount;
}

AVCaptureVideoPreviewLayer *_previewLayer;
AVCaptureSession *_captureSession;

AVCaptureVideoPreviewLayer *_BigpreviewLayer;
AVCaptureSession *_BigcaptureSession;

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [_ScanView setAlpha:0.0f];
    [_QrCode setAlpha:0.0f];
    _isReading = NO;
    _TakePhoto = NO;
    torchIsOn = FALSE;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"count"]!= nil)
    {
        imagecount = (int) [[NSUserDefaults standardUserDefaults] valueForKey:@"count"];
    }
    else
    {
        imagecount = 1;
    }
    
    _captureSession = nil;
    CapturedImage.image = nil;
    _backgroundView.layer.cornerRadius = _backgroundView.bounds.size.width/2;
    _backgroundView.layer.masksToBounds = YES;
    [_CameraOptionsView setHidden:YES];
    _backgroundView2.layer.cornerRadius = _backgroundView.bounds.size.width/2;
    _backgroundView2.layer.masksToBounds = YES;
    [_BackgroundFilterImage setHidden: YES];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    FilterArray = [userDefaults objectForKey:@"FilterArray"];

    dict = [NSMutableDictionary new];
    
    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    if(![[[defaults dictionaryRepresentation] allKeys] containsObject:@"filterDict"])
    {
        for (int i=0; i<FilterArray.count; i++)
        {
            
            NSString *FilterName = [[FilterArray valueForKey:@"name"]objectAtIndex:i];
            [dict setObject:[NSMutableArray new] forKey:FilterName];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"filterDict"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
   
    
    _filterTableView.delegate = self;
    _filterTableView.dataSource = self;
    [_filterTableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return FilterArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *VideoCellIdentifier = @"CustomEventCell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil]objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.titleLabel.text =  [NSString stringWithFormat:@"%@",[[FilterArray valueForKey:@"name"]objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *deviceName = [self.GlobelFucntion getDeviceModelName];
    
    FilterName  = [[FilterArray valueForKey:@"name"]objectAtIndex:indexPath.row];
    FilterId  = [[FilterArray valueForKey:@"id"]objectAtIndex:indexPath.row];

    if ([deviceName isEqualToString:@"iPhone 5"] || [deviceName isEqualToString:@"iPhone 5s"] || [deviceName isEqualToString:@"iPhone 5c"])
    {
        FilterImage = [[FilterArray objectAtIndex:indexPath.row] valueForKey:@"image_320_568"];
    }
    else if ([deviceName isEqualToString:@"iPhone 6"] || [deviceName isEqualToString:@"iPhone 6s"] || [deviceName isEqualToString:@"iPhone 7"])
    {
        FilterImage = [[FilterArray objectAtIndex:indexPath.row] valueForKey:@"image_375_667"];
    }
    else if ([deviceName isEqualToString:@"iPhone 6 Plus"] || [deviceName isEqualToString:@"iPhone 6s Plus"] || [deviceName isEqualToString:@"iPhone 7 Plus"] || [deviceName isEqualToString:@"iPhone 8 Plus"])
    {
        FilterImage = [[FilterArray objectAtIndex:indexPath.row] valueForKey:@"image_414_736"];
    }
    else if ([deviceName isEqualToString:@"iPhone X"])
    {
        FilterImage = [[FilterArray objectAtIndex:indexPath.row] valueForKey:@"image_375_812"];
    }
    else if ([deviceName isEqualToString:@"iPad"])
    {
        FilterImage = [[FilterArray objectAtIndex:indexPath.row] valueForKey:@"image_768_1024"];
    }
    else if ([deviceName isEqualToString:@"iPad Pro"])
    {
        FilterImage = [[FilterArray objectAtIndex:indexPath.row] valueForKey:@"image_1024_1366"];
    }
    else if ([deviceName isEqualToString:@"iPad Air"])
    {
        FilterImage = [[FilterArray objectAtIndex:indexPath.row] valueForKey:@"image_834_1112"];
    }
    [_BackgroundFilterImage sd_setImageWithURL:[NSURL URLWithString:FilterImage]
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [_ScanView setAlpha:1.0f];
                        _HeaderLabel.text = @"Scan";
                         Code = 0;
                         PicFrame = 0;
                        [_headerSaveButton setHidden:YES];
                        [_headerCancelButton setHidden:NO];
                        [self RunCamera];
                     }];
}


#pragma mark  SetUp Main Camera
-(void)RunCamera
{
    _BigcaptureSession = [[AVCaptureSession alloc] init];
    AVCaptureDevice * videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if(videoDevice == nil)
        assert(0);
    NSError *error;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice
                                                                        error:&error];
    if(error)
        assert(0);
    _BigcaptureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    [_BigcaptureSession addInput:input];
    _BigpreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_BigcaptureSession];
    _BigpreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [_BigpreviewLayer setFrame:CGRectMake(0, 0,
                                          self.CameraView.frame.size.width,
                                          self.CameraView.frame.size.height)];
    
    [self.CameraView.layer addSublayer:_BigpreviewLayer];
    
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
    [_BigcaptureSession addOutput:stillImageOutput];
    [_BigcaptureSession startRunning];
}

#pragma mark  Take Photo / Output


- (IBAction)TappedAction_TakePhotoView:(id)sender
{
    [UIView animateWithDuration:0.4
                     animations:^{
                         [self SetupCameraView];
                         
                     }];
    
}


- (IBAction)TappedAction_CancelScanView:(id)sender
{
    [UIView animateWithDuration:0.4
                     animations:^{
                         if (_TakePhoto == YES)
                         {
                             CapturedImage.image = nil;
                             _TakePhoto = NO;
                             [_backgroundView setAlpha:1.0f];
                             [_backgroundView2 setAlpha:1.0f];
                             _HeaderLabel.text = @"Scan";
                             [_BackgroundFilterImage setHidden:YES];
                             [_CameraOptionsView setHidden:YES];
                             [_headerSaveButton setHidden:YES];
                           
                             for (UIView *view in self.tabBarController.view.subviews)
                             {
                                 if ([view viewWithTag:1000]) {
                                     [view setHidden:NO];
                                 }
                             }
                             
                             [self.tabBarController.tabBar setHidden:NO];
                             Code = 0;
                             PicFrame = 0;
                         }
                         else
                         {
                             [_ScanView setAlpha:0.0f];
                         }
                     }];
    _isReading = NO;

}


- (IBAction)TappedAction_CaptureButton:(id)sender
{
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection)
        {
            break;
        }
    }
    
    NSLog(@"about to request a capture from: %@", stillImageOutput);
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (exifAttachments)
         {
         } else
         {
         }
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         
         CapturedImage.image = image;
         [_headerSaveButton setHidden: NO];
         [_CameraOptionsView setHidden:YES];
         if (Front == TRUE)
         {
             UIImage *flippedImage = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];
             CapturedImage.image = flippedImage;
         }
         PicFrame = 1;
     }];
}

-(void)SetupCameraView
{
    _TakePhoto = YES;
    Front = FALSE;
    CapturedImage.image = nil;
    [_captureSession stopRunning];
    _HeaderLabel.text = @"Take Photo";
    [_backgroundView setAlpha:0.0f];
    [_backgroundView2 setAlpha:0.0f];
    [_CameraOptionsView setHidden:NO];
    [_BackgroundFilterImage setHidden:NO];
    
    for (UIView *view in self.tabBarController.view.subviews)
    {
        if ([view viewWithTag:1000]) {
            [view setHidden:YES];
        }
    }
    [self.tabBarController.tabBar setHidden:YES];
}


#pragma mark  Managing QRCode Scanning Functions / Output

- (IBAction)TappedAction_QRcodeView:(id)sender
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         _nameTxt.text = @"";
                         _phoneTxt.text = @"";
                         _emailTxt.text = @"";
                         
                         [_CancelDoneButton setTitle:@"Cancel" forState:UIControlStateNormal];
                         [_QrCode setAlpha:1.0f];
                     }
                     completion:^(BOOL finished)
     {
         if (!_isReading)
         {
             [self startReading];
         }
         else
         {
             [self stopReading];
         }
         _isReading = !_isReading;
     }];
}


- (BOOL)startReading
{
    _captureSession = [[AVCaptureSession alloc] init];
    AVCaptureDevice * videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if(videoDevice == nil)
        assert(0);
    NSError *error;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice
                                                                        error:&error];
    if(error)
        assert(0);
    [_captureSession addInput:input];
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [_previewLayer setFrame:CGRectMake(0, 0,
                                       _camscanView.frame.size.width,
                                       _camscanView.frame.size.height)];
    
    [_camscanView.layer addSublayer:_previewLayer];
    [_captureSession startRunning];
    return YES;
}

-(void)stopReading
{
    [_captureSession stopRunning];
    _captureSession = nil;
    [_previewLayer removeFromSuperlayer];
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && [metadataObjects count] > 0)
    {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode])
        {
            NSArray *arr = [metadataObj.stringValue componentsSeparatedByString:@"\n"];
            NSString *beginatring = [arr objectAtIndex:0];
            if ([beginatring isEqualToString:@"BEGIN:VCARD"])
            {
                NSString *Name = [arr objectAtIndex:2];
                NSString *Phone = [arr objectAtIndex:4];
                NSString *Email = [arr objectAtIndex:6];
                
                NSString *NameString = [Name substringFromIndex:3];
                NSString *PhoneString = [Phone substringFromIndex:15];
                NSString *EmailString = [Email substringFromIndex:6];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^
                 {
                     _nameTxt.text = NameString;
                     _phoneTxt.text = PhoneString;
                     _emailTxt.text = EmailString;
                     [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                     [_CancelDoneButton setTitle:@"Done" forState:UIControlStateNormal];
                     _isReading = NO;
                 }];
                Code = 1;
            }
            else
            {
                [self.GlobelFucntion alert:@"Alert!" :@"Invalid QR Code" :self];
                Code = 1;
            }
        }
    }
}


- (IBAction)TappedAction_DismissQrCodeView:(id)sender
{
    if (PicFrame == 1 && Code == 0)
    {
        [_ScanView setHidden:NO];
        [_QrCode setHidden:YES];
    }
    else if (PicFrame == 1 && Code == 1)
    {
        [UIView animateWithDuration:0.2
                         animations:^{
                             [_QrCode setAlpha:0.0f];
                             _isReading = !_isReading;
                             [self stopReading];
                             [self RunCamera];
                             [self.tabBarController.tabBar setHidden:YES];
                             [_CameraOptionsView setHidden:YES];
                             
                         }];
    }
    else if (PicFrame == 0 || Code== 0 || Code == 1 )
    {
        UIButton *someButton = (UIButton*)sender;
        if ([someButton.titleLabel.text isEqualToString:@"Done"])
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Do you want to take photo?"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Yes"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [_QrCode setAlpha:0.0f];
                                    [self SetupCameraView];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"No"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         NSLog(@"Save Data");
                                         
                                         [UIView animateWithDuration:0.2
                                                          animations:^{
                                                              [_QrCode setAlpha:0.0f];
                                                              [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                                                              _isReading = NO;

                                                          }];
                                     }];
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else if ([someButton.titleLabel.text isEqualToString:@"Cancel"])
        {
            [UIView animateWithDuration:0.2
                             animations:^{
                                 [_QrCode setAlpha:0.0f];
                                 [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                                 _isReading = NO;
                                 [_BigcaptureSession startRunning];
                             }];
        }
    }
    
    
    
}

#pragma mark  Camera FrontBack / Flash Options

- (IBAction)TappedAction_FrontCam:(id)sender
{
    [_BigcaptureSession beginConfiguration];
    NSArray *inputs = [_BigcaptureSession inputs];
    
    
    for (AVCaptureInput *input in inputs)
    {
        [_BigcaptureSession removeInput:input];
    }
    AVCaptureDevice *newCamera = nil;
    if (Front == FALSE)
    {
        newCamera = [self cameraWithPosition:AVCaptureDevicePositionFront];
        Front = TRUE;
    }
    else
    {
        newCamera = [self cameraWithPosition:AVCaptureDevicePositionBack];
        Front = FALSE;
    }
    NSError *err = nil;
    AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:&err];
    if(!newVideoInput || err)
    {
        NSLog(@"Error creating capture device input: %@", err.localizedDescription);
    }
    else
    {
        [_BigcaptureSession addInput:newVideoInput];
    }
    [_BigcaptureSession commitConfiguration];
}

- (IBAction)turnTorchOn :(id)sender
{
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil)
    {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash])
        {
            [device lockForConfiguration:nil];
            if (torchIsOn == FALSE)
            {
                [device setFlashMode:AVCaptureFlashModeOn];
                torchIsOn = TRUE; //define as a variable/property if you need to know status
            } else {
                [device setFlashMode:AVCaptureFlashModeOff];
                torchIsOn = FALSE;
            }
            [device unlockForConfiguration];
        }
    }
}

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position) return device;
    }
    return nil;
}

- (IBAction)TappedAction_SavePhoto:(id)sender
{
    if (PicFrame == 1 && Code == 0)
    {
        NSLog(@"OnlyPicNoCode");
        UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Do you want to Scan Code?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [_QrCode setAlpha:1.0f];
                                 _isReading = NO;
                                 [self startReading];
                                 [self.tabBarController.tabBar setHidden:NO];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"No"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     _isReading = NO;
                                 }];
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else if (PicFrame == 1 && Code ==1)
    {
        NSLog(@"Both");
        
        NSString *nameString = _nameTxt.text;
        NSString *phoneTxt = _phoneTxt.text;
        NSString *emailTxt = _emailTxt.text;
        
        NSFileManager *fm = [NSFileManager defaultManager];
        NSArray *appSupportDir = [fm URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
        NSURL* dirPath = [[appSupportDir objectAtIndex:0] URLByAppendingPathComponent:FilterName];
        
        NSError*    theError = nil; //error setting
        if (![fm createDirectoryAtURL:dirPath withIntermediateDirectories:YES
                           attributes:nil error:&theError])
        {
            NSLog(@"not created");
        }
        
        UIGraphicsBeginImageContext(CGSizeMake(_BaseView.frame.size.width,_BaseView.frame.size.height));
        CGContextRef context = UIGraphicsGetCurrentContext();
        [_BaseView.layer renderInContext:context];
        UIImage *img_screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        imagecount++;
        [[NSUserDefaults standardUserDefaults] setInteger:imagecount forKey:@"count"];
        
        NSData *pngData = UIImagePNGRepresentation(img_screenShot);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.png",imagecount]]; //Add the file name
        [pngData writeToFile:filePath atomically:YES]; //Write the file

        
        NSString *imageCount = [NSString stringWithFormat:@"%d",imagecount];
        
        [self ShowProgress];
        
        NSDictionary * Dict = @{@"name":nameString,
                                @"phone":phoneTxt,
                                @"email":emailTxt,
                                @"filePath":filePath,
                                @"filterId":FilterId,
                                @"filterName":FilterName,
                                @"imageCount":imageCount
                                };
        [self addDataToUserDefalts:Dict];
    }
}

- (void)addDataToUserDefalts:(NSDictionary *)dic
{
    NSMutableDictionary *FilterdDic = [[[NSUserDefaults standardUserDefaults] objectForKey:@"filterDict"] mutableCopy];
    NSMutableArray *dataArray = [[FilterdDic objectForKey:FilterName] mutableCopy];
 
    // if (![dataArray containsObject:dic])
    {
        [dataArray addObject:dic];
    }
    [FilterdDic setObject:dataArray forKey:FilterName];
    [[NSUserDefaults standardUserDefaults] setObject:FilterdDic forKey:@"filterDict"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self HideProgress];
    [self.GlobelFucntion alert:@"Alert!" :@"Data Saved" :self];
    
    [self performSelector:@selector(ClearAll) withObject:nil afterDelay:1.0f];
}


-(void)ClearAll
{
    CapturedImage.image = nil;
    _TakePhoto = NO;
    [_backgroundView setAlpha:1.0f];
    [_backgroundView2 setAlpha:1.0f];
    _HeaderLabel.text = @"Scan";
    [_BackgroundFilterImage setHidden:YES];
    [_CameraOptionsView setHidden:YES];
    [_headerSaveButton setHidden:YES];
    [self.tabBarController.tabBar setHidden:NO];
    Code = 0;
    PicFrame = 0;
    [_QrCode setAlpha:0.0f];
    [_ScanView setAlpha:1.0f];
    for (UIView *view in self.tabBarController.view.subviews)
    {
        if ([view viewWithTag:1000]) {
            [view setHidden:NO];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [_ScanView setAlpha:0.0f];
    for (UIView *view in self.tabBarController.view.subviews)
    {
        if ([view viewWithTag:1000]) {
            [view setHidden:NO];
        }
    }
}


@end

