//
//  TrendingViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "EventDetailController.h"
#import "CollectionCell.h"
#import "TenderingDataObject.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Photos/Photos.h>

@interface EventDetailController ()
@end
@implementation EventDetailController


#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@",_picdataArray);
    [_fullView setAlpha:0.0f];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:NO];
    _TrendingCollectioView.delegate = self;
    _TrendingCollectioView.dataSource = self;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

# pragma mark  CollectionView Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section
{
    return _picdataArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *Cell = (CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Cell.contentView.frame = Cell.bounds;
    Cell.ServiceImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    Cell.layer.masksToBounds = NO;
    Cell.layer.borderColor = [UIColor whiteColor].CGColor;
    Cell.layer.borderWidth = 2.0f;
    Cell.layer.contentsScale = [UIScreen mainScreen].scale;
    Cell.layer.shadowOpacity = 0.55f;
    Cell.layer.shadowRadius = 2.0f;
    Cell.layer.shadowOffset = CGSizeZero;
    Cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:Cell.bounds].CGPath;
    Cell.layer.shouldRasterize = YES;
    
    NSString *filePath = [NSString stringWithFormat:@"%@.png",[[_picdataArray valueForKey:@"imageCount"] objectAtIndex:indexPath.row]];
    
    NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir  = [documentPaths objectAtIndex:0];
    NSString  *pngfile = [documentsDir stringByAppendingPathComponent:filePath];
   
    NSData *imageData = [NSData dataWithContentsOfFile:pngfile];
    img = [UIImage imageWithData:imageData];
    
    [Cell.ServiceImage setImage : img];
    Cell.ServiceImage.contentMode = UIViewContentModeScaleAspectFit;
    return Cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
     if(IS_IPHONE_5)
    {
        return UIEdgeInsetsMake(2, 2, 2, 2); // top, left, bottom, right
    }
    else
    {
        return UIEdgeInsetsMake(5, 10, 5, 10); // top, left, bottom, right
    }
    //    else if (IS_IPHONE_6_PLUS)
    //    {
    //        return UIEdgeInsetsMake(25, 25, 25, 25); // top, left, bottom, right
    //    }
    //    return UIEdgeInsetsMake(25, 20, 25, 20); // top, left, bottom, right
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *filePath = [NSString stringWithFormat:@"%@.png",[[_picdataArray valueForKey:@"imageCount"] objectAtIndex:indexPath.row]];
    
    NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir  = [documentPaths objectAtIndex:0];
    NSString  *pngfile = [documentsDir stringByAppendingPathComponent:filePath];
    
    NSData *imageData = [NSData dataWithContentsOfFile:pngfile];
    img = [UIImage imageWithData:imageData];
    
    
    
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [_fullView setAlpha:1.0f];
                         _picturePreview.image = img;

                         [self.tabBarController.tabBar setHidden:YES];
                         
                         for (UIView *view in self.tabBarController.view.subviews)
                         {
                             if ([view viewWithTag:1000]) {
                                 [view setHidden:YES];
                             }
                         }
                     }];
}

- (IBAction)TappedAction_backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)TappedAction_ExitFullView:(id)sender
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         [_fullView setAlpha:0.0f];
                         _picturePreview.image = nil;
                         [self.tabBarController.tabBar setHidden:NO];
                         for (UIView *view in self.tabBarController.view.subviews)
                         {
                             if ([view viewWithTag:1000]) {
                                 [view setHidden:NO];
                             }
                         }
                     }];
}


- (IBAction)tappedAction_saveImage:(id)sender
{
    [self ShowProgress];
        
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:_picturePreview.image];
        changeRequest.creationDate   = [NSDate date];
    } completionHandler:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"successfully saved");
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [self HideProgress];
                               [self.GlobelFucntion alert:@"Success" :@"Saved to camera roll" :self];
                           });
           
        }
        else {
            NSLog(@"error saving to photos: %@", error);
        }
    }];
}


-(void) viewDidDisappear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
