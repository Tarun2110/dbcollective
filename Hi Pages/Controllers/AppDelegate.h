//
//  AppDelegate.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate,UITabBarDelegate,UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) UIView *BackgroundView;

@property(nonatomic,retain)UITabBarController *tabBarController;
@property(nonatomic,retain)UITabBar *tabBar;
- (void)addTabbar;

@end

