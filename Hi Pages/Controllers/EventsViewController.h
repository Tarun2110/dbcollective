//
//  HomeViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CategoryObject.h"

@interface EventsViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *EventTableView;
}


@end
