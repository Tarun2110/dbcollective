//
//  BookingViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 08/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface ExportViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate>
{
}
@property (strong, nonatomic) IBOutlet UITableView *bookingTableView;

@end
