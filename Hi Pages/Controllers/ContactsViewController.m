//
//  ServicesViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "ContactsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CategoryObject.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomTableViewCell.h"



@interface ContactsViewController ()
@end
@implementation ContactsViewController
{
    NSMutableArray *ContactListArray;
    NSMutableDictionary *filterDict;
}

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:NO];
    ContactListArray = [NSMutableArray new];
    
    filterDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"filterDict"];
    NSMutableArray *Contacts = [[filterDict allValues] objectAtIndex:0];

    
    for (int i=0; i<Contacts.count; i++) {
        if (ContactListArray.count==0) {
            [ContactListArray addObject:[Contacts objectAtIndex:i]];
            
        }
        else if ([[ContactListArray valueForKey:@"name" ] containsObject:[[Contacts objectAtIndex:i] valueForKey:@"name"]])
        {
            NSLog(@"Same object");
        }
        else
        {
            [ContactListArray addObject:[Contacts objectAtIndex:i]];
        }
    }
    
    NSLog(@"%@",ContactListArray);
    
    _contactTableView.delegate = self;
    _contactTableView.dataSource = self;
    
    [_contactTableView reloadData];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ContactListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *VideoCellIdentifier = @"CustomEventCell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil]objectAtIndex:1];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.nameLabel.text =  [NSString stringWithFormat:@"Name: %@",[[ContactListArray valueForKey:@"name"]objectAtIndex:indexPath.row]];
    cell.contactLabel.text =  [NSString stringWithFormat:@"Phone Number: %@",[[ContactListArray valueForKey:@"phone"]objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
